public class Bookshelf {
    public static void main(String[] args){
        Book book1 = new Book();
        Book book2 = new Book();
        Book book3 = new Book();
        book1.setName ( "数据结构与算法" );
        book1.setAuthor ( "aaa" );
        book1.setPress ( "浙江大学出版社" );
        book1.setPubDate ( "2019.4" );
        book2.setName ( "JAVA" );
        book2.setAuthor ( "bbb" );
        book2.setPress ( "清华大学出版社" );
        book2.setPubDate ( "2019.4" );
        book3.setName ( "密码学" );
        book3.setAuthor ( "ccc" );
        book3.setPress ( "金城出版社" );
        book3.setPubDate ( "2019.4" );
        System.out.println(book1.toString ());
        System.out.println(book2.toString ());
        System.out.println(book3.toString ());
    }
}
