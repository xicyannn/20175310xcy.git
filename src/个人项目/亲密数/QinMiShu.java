public class QinMiShu {
    public static void main(String[] args) {

        for(int a=1;a<10000;a++)
        {
            int b=0,n=0;
            //计算数a的各因子，各因子之和存放于b
            for(int i=1;i<=a/2;i++) {
                if(a%i==0) {
                    b+=i;
                }
            }
            //计算b的各因子，各因子之和存于n
            for(int i=1;i<=b/2;i++) {
                if(b%i==0) {
                    n+=i;
                }
            }
            //若n=a，则a和b是一对亲密数
            if(n==a&&b!=n&&a<b) {
                System.out.println(a+"和"+b);
            }
        }
    }
}
