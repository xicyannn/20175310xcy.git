import java.util.*;
import java.lang.String;

public class timu {
    public static void main(String args[]) {
        System.out.println("请输入要生成的题目数：");
        Scanner reader1 = new Scanner(System.in);
        int x = reader1.nextInt();     //读取要生成的题目数x
        int count = 0;                   //表示答对的题数



        for (int i = 1; i <= x; i++) {
            Random random1 = new Random();   //生成随机数
            int w = random1.nextInt(2) + 1;   //生成1到2之间的随机数w，表示是否要用分数
            String Question1 = "";   //系统随机生成的表达式Question1
            System.out.println("题目" + i + ":");
            Random random = new Random();   //生成随机数
            int a = random.nextInt(5) + 1;   //生成1到5之间的随机数a，表示运算符个数
            if (w % 2 == 0) {         //如果w对2取余为0，不生成分数
                for (int j = 1; j <= a + 1; j++) {
                    int b = random.nextInt(10);   //生成0到9之间的随机数b，表示表达式中出现的随机数，共a+1个
                    if (j <= a) {
                        fuhao k = new fuhao();  //声明类fuhao的对象k
                        char q = k.yunsuanfu();   //产生一个运算符q

                        Question1 = Question1 + b + q;  //将生成的随机数和随机符号存入Question1中
                    } else {
                        Question1 = Question1 + b;    //将生成的随机数存入Question1中
                    }
                }

            }


            else if (w % 2 == 1) {        //如果w对2取余为1，生成分数
                for (int j = 1; j <= a + 1; j++) {
                    Rational r = new Rational();   //声明Rational的对象r
                    r.setNumerator(random.nextInt(3) + 1);//设置分子是1到3之间的随机数
                    r.setDenominator(random.nextInt(5) + 4);   //设置分母是4到8之间的随机数
                    int zi = r.getNumerator();   //将r的分子赋值给zi
                    int mu = r.getDenominator();   //将r的分母赋值给mu
                    if (j <= a) {
                        fuhao k = new fuhao();  //声明类fuhao的对象k
                        char q = k.yunsuanfu();   //产生一个运算符q
                        Question1 = Question1 + zi + '/' + mu +" " +q+" ";  //将生成的随机数和随机符号存入Question1中
                    } else {
                        Question1 = Question1 +zi + '/' + mu;    //将生成的随机数存入Question1中
                    }
                }


            }


            System.out.print(Question1 + "=");   //将生成的题目输出

            if (w % 2 == 0) {   //当不生成分数时
                sizeyunsuan t = new sizeyunsuan();  //声明类sizeyunsuan的对象t
                float dui = t.calrp(t.getrp(Question1));  //计算正确答案dui
                Scanner reader2 = new Scanner(System.in);
                float answer = reader2.nextFloat();     //读取用户输入的答案
                if (answer == dui) {
                    System.out.println("回答正确！");
                    count++;   //count代表正确题数
                } else {
                    System.out.println("回答错误！正确答案是：" + dui);
                }
            }
            else if (w % 2 == 1) {    //当生成分数时
                MyDC evaluator = new MyDC();    //声明MyDC的对象evaluator
                sizeyunsuan l = new sizeyunsuan();  //声明类sizeyunsuan的对象l
                String Question=l.fenshurp(Question1);   //将Question1由中缀式改成后缀式
                String dui ="";   //代表正确答案dui
                dui =  dui + evaluator.evaluate(Question);
                Scanner reader2 = new Scanner(System.in);
                String answer = reader2.nextLine();     //读取用户输入的答案
                if(answer.equals(dui)){
                    System.out.println("回答正确！");
                    count++;      //count代表正确题数
                }
                else if(!answer.equals(dui)){
                    System.out.println("回答错误！正确答案是：" + dui);
                }





            }

        }
        float lv = count * 100 / x;     //正确率
        System.out.println("测试结束！正确率为：" + lv + "%");
    }
}
