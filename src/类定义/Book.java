public class Book {
   String bookName;
   String author; 
   String press;
   String pubDate;

   public String getName() {
      return bookName;
   }

   public void setName(String name) {
      bookName= name;
   }

   public String getAuthor() {
      return author;
   }

   public void setAuthor(String author) {
      this.author = author;
   }

   public String getPress() {
      return press;
   }

   public void setPress(String press) {
      this.press = press;
   }

   public String getPubDate() {
      return pubDate;
   }

   public void setPubDate(String date) {
      pubDate = date;
   }

   Book (){    //自定义构造方法
      bookName = "xxx";
      author = "qwerty";
      press ="xx出版社";
      pubDate = "20xx.x";
   }

   public String toString() {     //覆盖了父类Object中的toString方法。
      return "Book\n" +
              "书名：" + bookName + "\n" +
              "作者：" + author + "\n" +
              "出版社：" + press + "\n" +
              "出版时间：" + pubDate + "\n";
   }

   public boolean equals(Object obj) {    //覆盖了父类Object中的equlas方法。
      if (this == obj)  //判断是否为同一对象
         return true;
      if (obj == null )   //判断是否为空
         return false;
      if (getClass() != obj.getClass())   //判断是否属于同一个类
         return false;
      Book book = (Book) obj;   ////如果类型相同，比较内容
      if (bookName == null) {
         if (book.bookName != null)
            return false;
      } else if (!bookName.equals(book.bookName))
         return false;
      if (author == null) {
         if (book.author != null)
            return false;
      } else if (!author.equals(book.author))
         return false;
      if (press == null) {
         if (book.press != null)
            return false;
      } else if (!press.equals(book.press))
         return false;
      if (pubDate == null) {
         if (book.pubDate != null)
            return false;
      } else if (!pubDate.equals(book.pubDate))
         return false;
      return true;
   }

   public int hashCode() {    //重写hashcode
      final int prime = 31;
      int result = 1;
      result = prime * result + ((bookName == null) ? 0 : bookName.hashCode());
      result = prime * result + ((author == null) ? 0 : author.hashCode());
      result = prime * result + ((press == null) ? 0 : press.hashCode());
      result = prime * result + ((pubDate == null) ? 0 : pubDate.hashCode());
      return result;
   }

}
