import java.util.*;
public class MyList {
	public static void main(String [] args) {
		//选用合适的构造方法，用你学号前后各两名同学的学号创建四个结点
		List<String> list=new LinkedList<String>();
		list.add("5308");
		list.add("5309");
		list.add("5311");
		list.add("5312");
		//把上面四个节点连成一个没有头结点的单链表
		
		//遍历单链表，打印每个结点的
		System.out.println("插入前：");
		Iterator<String> iter1=list.iterator();
		while(iter1.hasNext()){
			String te=iter1.next();
			System.out.println(te);
		}

		//把你自己插入到合适的位置（学号升序）
		list.add(2,"5310");

		//遍历单链表，打印每个结点的
		System.out.println("插入后：");
		Iterator<String> iter2=list.iterator();
		while(iter2.hasNext()){
			String te=iter2.next();
			System.out.println(te);
		}

		//从链表中删除自己
		list.remove(2);

		//遍历单链表，打印每个结点的
		System.out.println("删除后：");
		Iterator<String> iter3=list.iterator();
		while(iter3.hasNext()){
			String te=iter3.next();
			System.out.println(te);
		}
	}
}
