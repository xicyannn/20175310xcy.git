import java.io.*;
public class MyCP {
    public static void main(String[] args) {
        String a = args[0];
        File xcy1 = new File(args[1]);
        File xcy2 = new File(args[2]);
        try {
            Reader in = new FileReader(xcy1);
            BufferedReader bufferedReader = new BufferedReader(in);
            String yuan = bufferedReader.readLine();//读取源文件中的字符串
            String target = "";
            if(a.equals("-tx")){             //将十进制转化成二进制
                target = Integer.toBinaryString(Integer.parseInt(yuan));
            }
            else if(a.equals("-xt")){        //将二进制转化成十进制
                target = Integer.parseInt(yuan,2)+"";
            }
            Writer out = new FileWriter(xcy2);  //写入目标文件
            BufferedWriter bufferedWriter = new BufferedWriter(out);
            bufferedWriter.write(target);
            bufferedReader.close();
            bufferedWriter.close();
        } catch (Exception e) {
            System.out.println("Error!"+e);
        }
    }
}
