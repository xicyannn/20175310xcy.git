import java.util.*;

public class HuiWenShu {
  public static void main(String[] args) {
   Scanner sc = new Scanner(System.in);
   System.out.println("请输入一个数字: ");
   String word = sc.next();
   int i = word.length();
   int j = 0;
   while (j <= (i / 2) -1 && word.charAt(j) == word.charAt(i - j - 1)) {
       j++;
   }
   if (j == i / 2) {
       System.out.println(word+"是回文数");
   }
   else {
       System.out.println(word+"不是回文数");
   }
   }
}
