import java.util.Stack; 
import java.util.Scanner;  
 
public class sizeyunsuan {
 
    static Stack<Character> op = new Stack<>();
     
     
    //计算后缀式的值 
    public static float calrp(String rp){//参数rp：后缀式
        Stack<Float> v = new Stack<>();
        char[] arr = rp.toCharArray();
        int len = arr.length;
        for(int i = 0; i < len; i++){
            Character ch = arr[i];
 
            // 如果是操作数，则推到堆栈
            if(ch >= '0' && ch <= '9') v.push(Float.valueOf(ch - '0'));
 
            // 如果是运算符，则计算结果
            // 在堆栈中有前2个操作数的情况下，将结果推送到堆栈中
            else v.push(getv(ch, v.pop(), v.pop()));
        }
        return v.pop();//返回值return：表达式结果
    }
 



   //将中缀式转换为后缀式
    public static String getrp(String s){//参数s：中缀形式的字符串
         char[] arr = s.toCharArray();
         int len = arr.length;
         String out = "";
 
         for(int i = 0; i < len; i++){   //从左到右扫描中缀式
             char ch = arr[i];
             if(ch == ' ') continue;
             if(ch >= '0' && ch <= '9') {// 如果是操作数，则直接输出
                 out+=ch;
                 continue;
             }
 
             if(ch == '+' || ch == '-'){//如果遇到“+”或“-”，则从堆栈中弹出运算符，直到遇到“（”，然后输出，并进栈。
                 while(!op.empty() && (op.peek() != '(')) 
                     out+=op.pop();
                 op.push(ch);
                 continue;
             }
 
             
             if(ch == '*' || ch == '/'){//如果是“*”或“/”，则退栈并输出，直到优先级较低或“（”将运算符进栈
                 while(!op.empty() && (op.peek() == '*' || op.peek() == '/')) 
                     out+=op.pop();
                 op.push(ch);
                 continue;
             }

             if(ch == '(') op.push(ch);//如果遇到“（”，则直接进栈
 
    
             
             if(ch == ')'){ //如果遇到“）”一直退栈输出，直到退到“（”，弹出“（”
                 while(!op.empty() && op.peek() != '(') 
                     out += op.pop();
                 op.pop();
                 continue;
             }
         }
         while(!op.empty()) out += op.pop();
         return out;//返回值return：后缀形式的字符串
    }
 



    public static Float getv(char op, Float f1, Float f2){
        if(op == '+') return f2 + f1;
        else if(op == '-') return f2 - f1;
        else if(op  == '*') return f2 * f1;
        else if(op == '/') return f2 / f1;
        else return Float.valueOf(-0);
    }
 
     
    public static void main(String[] args){
     try{        
        Scanner reader=new Scanner(System.in);
        System.out.println("请输入表达式：");
        String exp=reader.nextLine();
        System.out.println(calrp(getrp(exp)));
     }
     catch (Exception IOException)           {                                                            
        System.out.println("输入格式错误！");            
     }                
    }
 
}

