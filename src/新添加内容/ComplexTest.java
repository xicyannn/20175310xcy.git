import junit.framework.TestCase;
import org.junit.Test;
/**
 * @author gong9
 */
public class ComplexTest extends TestCase {
    Complex a = new Complex(0, 5);
    Complex b = new Complex(-5, -1);
    Complex c = new Complex(6, 1);

    @Test
    public void testgetRealPart() throws Exception {
        assertEquals(0.0, a.getRealPart(a));
        assertEquals(-5.0, b.getRealPart(b));
        assertEquals(6.0, c.getRealPart(c));
    }

    @Test
    public void testgetImagePart() throws Exception {
        assertEquals(5.0, a.getImagePart(a));
        assertEquals(-1.0, b.getImagePart(b));
        assertEquals(1.0, c.getImagePart(c));
    }

    @Test
    public void testComplexAdd() throws Exception {
        assertEquals("-5.0+4.0i", a.complexAdd(b).toString());
        assertEquals("6.0+6.0i", a.complexAdd(c).toString());
        assertEquals("1.0", b.complexAdd(c).toString());
    }

    @Test
    public void testComplexSub() throws Exception {
        assertEquals("5.0+6.0i", a.complexSub(b).toString());
        assertEquals("-6.0+4.0i", a.complexSub(c).toString());
        assertEquals("-11.0-2.0i", b.complexSub(c).toString());
    }


}