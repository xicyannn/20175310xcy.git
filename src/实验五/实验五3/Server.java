import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
public class Server {
    public static void main(String[] args) {
        String s;           //s是接收过来的后缀表达式的字符串
        ServerSocket serverForClient = null;
        Socket socketOnServer = null;
        DataOutputStream out = null;
        DataInputStream in = null;
        try{
            serverForClient = new ServerSocket(2010);   //与客户端的2010端口一致
        }
        catch(IOException e1){
            System.out.println(e1);
        }
        try{
            System.out.println("等待客户呼叫");
            socketOnServer = serverForClient.accept();
            out = new DataOutputStream(socketOnServer.getOutputStream());
            in = new DataInputStream(socketOnServer.getInputStream());
            int length = Integer.valueOf(in.readUTF());
            byte ctext[] = new byte[length];
            for(int i =0;i<length;i++){
                String temp = in.readUTF();
                ctext[i] = Byte.parseByte(temp);
            }
            FileInputStream f2 = new FileInputStream("keykb1.dat");
            int num2 = f2.available();
            byte[] keykb = new byte[num2];
            f2.read(keykb);
            SecretKeySpec k = new SecretKeySpec(keykb, "DESede");
            Cipher cp = Cipher.getInstance("DESede");
            cp.init(Cipher.DECRYPT_MODE, k);
            byte[] ptext = cp.doFinal(ctext);
            String p = new String(ptext,"UTF8");
            System.out.println("被解密的后缀表达式：" + p);
            System.out.println("计算后缀表达式" + p);
            MyDC mydc = new MyDC();
            float resultNumber = mydc.calrp(p);       //计算结果
            System.out.println("服务器端计算的结果是："+resultNumber);
            Float I = new Float(resultNumber);
            s = I.toString();
            /*把计算结果以字符串的形式发送给客户端*/
            out.writeUTF(s);
        } catch (Exception e) {
            System.out.println("客户已断开" + e);
        }
    }
}
