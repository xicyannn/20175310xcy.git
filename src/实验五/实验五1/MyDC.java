import java.util.Stack;
import java.util.Scanner;

public class MyDC {

  static Stack<Character> op = new Stack<>();


  //计算后缀式的值
  public static float calrp(String rp){//参数rp：后缀式
    Stack<Float> v = new Stack<>();
    char[] arr = rp.toCharArray();
    int len = arr.length;
    for(int i = 0; i < len; i++){
      Character ch = arr[i];

      // 如果是操作数，则推到堆栈
      if(ch >= '0' && ch <= '9') v.push(Float.valueOf(ch - '0'));

        // 如果是运算符，则计算结果
        // 在堆栈中有前2个操作数的情况下，将结果推送到堆栈中
      else v.push(getv(ch, v.pop(), v.pop()));
    }
    return v.pop();//返回值return：表达式结果
  }


  public static Float getv(char op, Float f1, Float f2){
    if(op == '+') return f2 + f1;
    else if(op == '-') return f2 - f1;
    else if(op  == '*') return f2 * f1;
    else if(op == '/') return f2 / f1;
    else return Float.valueOf(-0);
  }


  public static void main(String[] args){
    MyBC bc=new MyBC();
    try{
      Scanner reader=new Scanner(System.in);
      System.out.println("请输入表达式：");
      String exp=reader.nextLine();
      System.out.println(calrp(bc.getrp(exp)));
    }
    catch (Exception IOException)           {
      System.out.println("输入格式错误！");
    }
  }

}
