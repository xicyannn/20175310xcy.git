import java.util.*;
class Student {
    private int id;   //表示学号
    private String name;//表示姓名
    double computer_score;//表示计算机课程的成绩
    private double english_score;//表示英语课的成绩
    private double maths_score;//表示数学课的成绩
    private double total_score;// 表示总成绩

    public Student(int id, String name,double computer_score,
                   double english_score,double maths_score) {
        this.id = id;
        this.name = name;
        this.computer_score = computer_score;
        this.english_score = english_score;
        this.maths_score = maths_score;
    }


    public int getId() {
        return id;
    }//获得当前对象的学号，



    public double getTotalScore() {
        total_score=computer_score + maths_score + english_score;
        return total_score;
    }// 计算Computer_score, Maths_score 和English_score 三门课的总成绩。



    @Override
    public String toString() {
        return "  "+name+"  "+id+"  "+computer_score +"  "+ maths_score+"  " + english_score+"  "+total_score;
    }

}



class sortTotalScore implements Comparator<Student> {
    @Override
    public int compare(Student a, Student b) {
        return (int)( a.getTotalScore() - b.getTotalScore());
    }
}

class sortID implements Comparator<Student> {

    @Override
    public int compare(Student a, Student b) {
        return (int)(a.getId() - b.getId());
    }

}

