import java.util.*;
public class SiFangDingLi {
    public static void main(String[] args) {  
        Scanner scan = new Scanner(System.in);
            int number;  
            System.out.printf("请输入一个整数：");
            number = scan.nextInt();  
            int a[] = { 0, 0, 0, 0 };  
            int r = f(number, a, 0);
            if (r==1){
                System.out.printf(number+"可分解成"+ a[0]+" "+ a[1]+" "+ a[2]+ " "+a[3]+"的平方和");
            }
            else{
                System.out.printf(number+"不可分解");
            }
    }
    public static int f(int n, int a[], int idx) {
        if (n==0) {
            return 1;
        }
        if (idx == 4) {
            return 0;
        }
        for (int i = (int) Math.sqrt(n); i >= 1; i--) {
            a[idx] = i;
            if (f(n-i*i, a, idx+1) == 1) {
                return 1;
            }
        }
        return 0;
    }
}
