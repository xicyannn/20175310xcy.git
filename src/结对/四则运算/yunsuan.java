import java.util.*;
import java.util.Stack;
import java.lang.String;
public class yunsuan {
        public static void main (String args[ ]) {
        System.out.println("请输入要生成的题目数：");
        Scanner reader1=new Scanner(System.in);
        int x = reader1.nextInt();     //读取要生成的题目数x
        int count=0;   //表示答对的题数
        for(int i=1;i<=x;i++){
            String Question = new String("");   //系统随机生成的表达式
            System.out.println("题目"+i+":");
            Random random=new Random();   //生成随机数
            int a = random.nextInt(5)+1;   //生成1到5之间的随机数a，表示运算符个数
            for (int j=1;j<=a+1;j++) {
                int b = random.nextInt(10) ;   //生成0到9之间的随机数b，表示表达式中出现的随机数，共a+1个
                System.out.print(b);   //将生成的随机数输出
                if (j <= a) {
                    fuhao k = new fuhao();  //声明类fuhao的对象k
                    char q = k.yunsuanfu();   //产生一个运算符q
                    System.out.print(q);     //将生成的随机符号输出
                    Question = Question + b +q;  //将生成的随机数和随机符号存入Question中
                }
                else{
                    Question = Question + b;    //将生成的随机数存入Question中
                }
            }
            System.out.println("=");
            sizeyunsuan l=new sizeyunsuan();  //声明类sizeyunsuan的对象l
            float right =  l.calrp(l.getrp(Question));  //正确答案right
            Scanner reader2=new Scanner(System.in);
            double answer = reader2.nextDouble();     //读取用户输入的答案
            if (answer==right){
                System.out.println("回答正确！");
                count++;
            }
            else{
                System.out.println("回答错误！正确答案是："+right);
            }


        }
        float lv=count*100/x;     //正确率
        System.out.println("测试结束！正确率为："+lv+"%");
    }
}


class fuhao{  //随机产生运算符
    char p;
    char yunsuanfu(){
        Random random=new Random();
        int m = random.nextInt(4)+1;  ////生成1到4之间的随机数m，分别表示+-*÷四则运算
        switch(m){
            case 1:
                p='+';
                break;
            case 2:
                p='-';
                break;
            case 3:
                p='*';
                break;
            case 4:
                p='÷';
                break;
        }
        return p;
    }
}


class sizeyunsuan{
    static Stack<Character> op = new Stack<>();


    //计算后缀式的值
    public static float calrp(String rp){//参数rp：后缀式
        Stack<Float> v = new Stack<>();
        char[] arr = rp.toCharArray();
        int len = arr.length;
        for(int i = 0; i < len; i++){
            Character ch = arr[i];

            // 如果是操作数，则推到堆栈
            if(ch >= '0' && ch <= '9') v.push(Float.valueOf(ch - '0'));

                // 如果是运算符，则计算结果
                // 在堆栈中有前2个操作数的情况下，将结果推送到堆栈中
            else v.push(getv(ch, v.pop(), v.pop()));
        }
        return v.pop();//返回值return：表达式结果
    }




    //将中缀式转换为后缀式
    public static String getrp(String s){//参数s：中缀形式的字符串
        char[] arr = s.toCharArray();
        int len = arr.length;
        String out = "";

        for(int i = 0; i < len; i++){   //从左到右扫描中缀式
            char ch = arr[i];
            if(ch == ' ') continue;
            if(ch >= '0' && ch <= '9') {// 如果是操作数，则直接输出
                out+=ch;
                continue;
            }

            if(ch == '+' || ch == '-'){//如果遇到“+”或“-”，则从堆栈中弹出运算符，直到遇到“（”，然后输出，并进栈。
                while(!op.empty() && (op.peek() != '('))
                    out+=op.pop();
                op.push(ch);
                continue;
            }


            if(ch == '*' || ch == '÷'){//如果是“*”或“÷”，则退栈并输出，直到优先级较低或“（”将运算符进栈
                while(!op.empty() && (op.peek() == '*' || op.peek() == '÷'))
                    out+=op.pop();
                op.push(ch);
                continue;
            }

            if(ch == '(') op.push(ch);//如果遇到“（”，则直接进栈



            if(ch == ')'){ //如果遇到“）”一直退栈输出，直到退到“（”，弹出“（”
                while(!op.empty() && op.peek() != '(')
                    out += op.pop();
                op.pop();
                continue;
            }
        }
        while(!op.empty()) out += op.pop();
        return out;//返回值return：后缀形式的字符串
    }




    public static Float getv(char op, Float f1, Float f2){
        if(op == '+') return f2 + f1;
        else if(op == '-') return f2 - f1;
        else if(op  == '*') return f2 * f1;
        else if(op == '÷') return f2 / f1;
        else return Float.valueOf(-0);
    }

}
