import java.util.Stack;

public class MyBC {
    static Stack<Character> op = new Stack<>();
    //将中缀式转换为后缀式
    public  String getrp(String s){//参数s：中缀形式的字符串
        char[] arr = s.toCharArray();
        int len = arr.length;
        String out = "";

        for(int i = 0; i < len; i++){   //从左到右扫描中缀式
            char ch = arr[i];
            if(ch == ' ') continue;
            if(ch >= '0' && ch <= '9') {// 如果是操作数，则直接输出
                out+=ch;
                continue;
            }

            if(ch == '+' || ch == '-'){//如果遇到“+”或“-”，则从堆栈中弹出运算符，直到遇到“（”，然后输出，并进栈。
                while(!op.empty() && (op.peek() != '('))
                    out+=op.pop();
                op.push(ch);
                continue;
            }


            if(ch == '*' || ch == '/'){//如果是“*”或“/”，则退栈并输出，直到优先级较低或“（”将运算符进栈
                while(!op.empty() && (op.peek() == '*' || op.peek() == '/'))
                    out+=op.pop();
                op.push(ch);
                continue;
            }

            if(ch == '(') op.push(ch);//如果遇到“（”，则直接进栈



            if(ch == ')'){ //如果遇到“）”一直退栈输出，直到退到“（”，弹出“（”
                while(!op.empty() && op.peek() != '(')
                    out += op.pop();
                op.pop();
                continue;
            }
        }
        while(!op.empty()) out += op.pop();
        return out;//返回值return：后缀形式的字符串
    }


}
