public class WanQuanShu {
    public static void main(String[] args) {
        System.out.println( "1000以内的完全数有：");
        for (int i = 1; i <= 1000; i++) {
            // 表示因子之和
            int sum = 0;
            for (int j = 1; j < i / 2 + 1; j++) {
                // 如果i能被j整除
                if (i % j == 0) {
                    //sum就加上i的因子j
                    sum += j;
                }
            }
            // 如果sum与i相等，说明是完全数。
            if (sum == i) {
                System.out.println(i);
            }
        }
    }
}
