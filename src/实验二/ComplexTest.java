import junit.framework.TestCase;
import org.junit.Test;
public class ComplexTest extends TestCase {
    Complex b = new Complex(2,6);
    Complex c = new Complex(1,-3);
    @Test
    public void testComplexAdd(){
        assertEquals("3.0+3.0i",b.ComplexAdd(c)+"");
    }
    @Test
    public void testComplexSub(){
        assertEquals("1.0+9.0i",b.ComplexSub(c)+"");
    }
    @Test
    public void testComplexMulti(){
        assertEquals("20.0",b.ComplexMulti(c)+"");
    }
    @Test
    public void testComplexDiv(){
        assertEquals("-4.0+3.0i",b.ComplexDiv(c)+"");
    }
}
