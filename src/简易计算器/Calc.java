public class Calc {
    public static void main(String [] args) {

        int result = 0;



        if (args.length != 3) {
            System.out.println("Usage: java Calc operato1 operand(+ - * / %) operator2");
        }

        //+ - x / 和%运算
        switch (args[1]) {
            case "+":
                result = Integer.parseInt(args[0]) + Integer.parseInt(args[2]);
                break;
            case "-":
                result = Integer.parseInt(args[0]) - Integer.parseInt(args[2]);
                break;
            case "x":
                result = Integer.parseInt(args[0]) * Integer.parseInt(args[2]);
                break;
            case "/":
                result = Integer.parseInt(args[0]) / Integer.parseInt(args[2]);
                break;
            case "%":
                result = Integer.parseInt(args[0]) % Integer.parseInt(args[2]);
                break;
            default:
                System.out.println("Usage: java Calc operato1 operand(+ - * / %) operator2");
                break;

        }

        System.out.println(args[0] + " " + args[1] + " " + args[2] + " = " + result);


    }
}
