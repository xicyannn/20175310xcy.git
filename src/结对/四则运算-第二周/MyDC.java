import java.util.StringTokenizer;
import java.util.Stack;
import java.lang.*;

public class MyDC
{
    /** constant for addition symbol */
    private final char ADD = '+';
    /** constant for subtraction symbol */
    private final char SUBTRACT = '-';
    /** constant for multiplication symbol */
    private final char MULTIPLY = '*';
    /** constant for division symbol */
    private final char DIVIDE = '÷';
    /** the stack */
    private Stack<String> stack;//存放操作数的栈
    private Rational  op1, op2;   //r1,r2

    public MyDC()   {
        stack = new Stack<String>();
    }

    public String evaluate (String expr)
    {
        String r1, r2, result = null;
        String token;
        StringTokenizer tokenizer = new StringTokenizer (expr);  //划分表达式
        while (tokenizer.hasMoreTokens())
        {
            token = tokenizer.nextToken();//将算数表达式分解

            if (isOperator(token))  //见下方isOperateor方法,当是运算符的时候进入if语句
            {
                r2 = stack.pop();
                op2=tranIntoRational(r2);
                r1 = stack.pop();
                op1=tranIntoRational(r1);
                result = evalSingleOp (token.charAt(0), op1, op2);//见下方evaSingleOp方法
                result= stack.push (result);//将计算结果压栈
            }
            else {
                stack.push (token);//操作数入栈
            }

        }

        return result;//输出结果
    }

    private boolean isOperator (String token)//判断是否为运算符,注意用equal语句比较字符串
    {
        return ( token.equals("+") || token.equals("-") ||
                token.equals("*") || token.equals("÷") );
    }

    private String evalSingleOp (char operation, Rational op1, Rational op2)   //分数运算
    {
        Rational result=new Rational();
        result.setNumerator(0);
        result.setDenominator(1);
        switch (operation)
        {
            case ADD:
                result = op1.add(op2);
                break;
            case SUBTRACT:
                result = op1.sub(op2);
                break;
            case MULTIPLY:
                result = op1.muti(op2);
                break;
            case DIVIDE:
                result = op1.div(op2);
                break;
            default:
                System.out.println("Error!");
        }
        return result.toString();
    }


    public Rational tranIntoRational (String s){   //将String类型转换成Rational类型
        Rational r = new Rational();
        String token1, token2;
        StringTokenizer tokenizer1 = new StringTokenizer(s, "/");//把操作数以"/"为标记分割开来
        token1 = tokenizer1.nextToken();    //分子
        if (tokenizer1.hasMoreTokens()) {//如果有第二个元素就把token1放在分子的位置，token2放在分母的位置
            token2 = tokenizer1.nextToken();    //分母

            r.setNumerator(Integer.parseInt(token1));   //设置分子
            r.setDenominator(Integer.parseInt(token2));   //设置分母
        }
        else {//如果没有第二个元素就把token1放在分子的位置，分母固定为1
            r.setNumerator(Integer.parseInt(token1));
            r.setDenominator(1);
        }
        return r;
    }
}
