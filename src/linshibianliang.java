public class linshibianliang {
        public static void main(String[] args) {
            //定义一个数组，比如
            int arr[] = {1,2,3,4,5,6,7,8};

            //打印原始数组的值
            for(int i:arr){
                System.out.print(i + " ");
            }
            System.out.println();

            // 添加代码删除上面数组中的5
            int locate = 0;
            for(int i:arr){
                if(arr[i] == 5){
                    locate = i;
                    break;
                }
            }

            for(int i=locate +1;i<arr.length;i++){
                arr[i-1] = arr[i];
            }
            arr[arr.length-1] = 0;

            //打印出 1 2 3 4 6 7 8 0
            for(int i:arr){
                System.out.print(i + " ");
            }

            System.out.println();

            // 添加代码再在4后面5
            for(int i:arr){
                if(arr[i] == 4){
                    locate = i;
                    break;
                }
            }

            for(int i=arr.length-1;i>locate+1;i--){
                arr[i] = arr[i-1];
            }
            arr[locate+1] = 5;

            //打印出 1 2 3 4 5 6 7 8
            for(int i:arr){
                System.out.print(i + " ");
            }
            System.out.println();
        }
}
