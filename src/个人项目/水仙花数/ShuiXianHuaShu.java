public class ShuiXianHuaShu {
    public static void main(String[] args) {
        //水仙花数的个数
        int x = 0;
        for(int i=100;i<=999;i++){
            //百位数
            int b = i/100;
            //十位数
            int s = (i-100*b)/10;
            //个位数
            int g = (i-s*10-b*100);
            if(i==g*g*g+s*s*s+b*b*b){
                //符合水仙花数条件时x+1;
                x++;
                //输出水仙花数
                System.out.print(i+" ");
        }
        }
        System.out.println();
        //输出水仙花数的总数
        System.out.println("水仙花数总共有"+x+"个");
    }
}
