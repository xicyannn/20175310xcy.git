import junit.framework.TestCase;
import org.junit.Test;
public class ComplexTest extends TestCase {
    Complex a = new Complex(0, 5);
    Complex b = new Complex(-5, -1);
    Complex c = new Complex(6,1);

    @Test
    public void testComplexAdd() throws Exception {
        assertEquals("-5.0+4.0i", a.ComplexAdd(b).toString());
        assertEquals("6.0+6.0i", a.ComplexAdd(c).toString());
        assertEquals("1.0", b.ComplexAdd(c).toString());
    }
    @Test
    public void testComplexSub() throws Exception {
        assertEquals("5.0+6.0i", a.ComplexSub(b).toString());
        assertEquals("-6.0+4.0i", a.ComplexSub(c).toString());
        assertEquals("-11.0-2.0i", b.ComplexSub(c).toString());
    }
    @Test
    public void testComplexMulti() throws Exception {
        assertEquals("5.0-25.0i", a.ComplexMulti(b).toString());
        assertEquals("-5.0+30.0i", a.ComplexMulti(c).toString());
        assertEquals("-29.0-11.0i", b.ComplexMulti(c).toString());
    }
    @Test
    public void testComplexComplexDiv() throws Exception {
        assertEquals("-0.2-1.0i", b.ComplexDiv(a).toString());
        assertEquals("0.1+0.8i", a.ComplexDiv(c).toString());
        assertEquals("-0.8-0.3i", b.ComplexDiv(c).toString());
    }

}
