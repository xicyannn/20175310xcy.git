/**
 * @author 20175310奚晨妍
 * @date 2019/04/29
 */
public class CodeStandard {
    public CodeStandard() {
        super();
    }

    public static void main(String[] args) {
        int n = 20;
        StringBuffer buffer = new StringBuffer();
        buffer.append('S');
        buffer.append("tringBuffer");
        System.out.println(buffer.charAt(1));
        System.out.println(buffer.capacity());
        System.out.println(buffer.indexOf("tring"));
        System.out.println("buffer = " + buffer.toString());
        if (buffer.capacity() < n) {
            buffer.append("1234567");
        }
        for (int i = 0; i < buffer.length(); i++) {
            System.out.println(buffer.charAt(i));
        }
    }
}
