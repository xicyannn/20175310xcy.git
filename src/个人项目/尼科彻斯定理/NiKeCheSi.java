import java.util.Scanner;
public class NiKeCheSi {
    public static void main(String[] args) {
        System.out.println("请输入一个数：");
        Scanner s = new Scanner(System.in) ;
        int n = s.nextInt() ;
        String res = "" ;
        for (int j = n * (n - 1) / 2; j < n * (n + 1) / 2; j++) {
            if(j != n * (n + 1) / 2 -1){
                res += (j*2+1) + "+" ;
            }else{
                res += (j*2+1) ;
            }            
        }            
        System.out.println(n+"的三次方等于"+res);
    }
}
