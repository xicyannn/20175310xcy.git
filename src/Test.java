class CPU{
    static int speed;
    
    void  setSpeed(int m){
        speed = m;
    }
    CPU(){
        speed=speed*3;
    }
    int getSpeed(){
        
        return speed;
    }
    
}
class PC {
    CPU cpu;
    HardDisk hardDisk;
    void setCPU(CPU c) {
        cpu = c;
    }
    
    void setHardDisk(HardDisk h) {
        hardDisk = h;
    }

    void show(){
        System.out.println("speed ="+cpu.getSpeed()+"      HardDisk ="+hardDisk.getAmount());
    }
    
}
class HardDisk{
    static int Amount;
    void setAmount(int d){
        Amount = d;
    }
    HardDisk(){
        Amount = Amount*3;
    }
    int getAmount(){
        return Amount;
    }
}
public class Test {
    public static void main(String[] args) {
        CPU cpu = new CPU();
        HardDisk disk = new HardDisk();
        cpu.setSpeed(2200);
        disk.setAmount(200);
        PC pc = new PC();
        pc.setCPU(cpu);
        pc.setHardDisk(disk);
        pc.show();
        System.out.println("cpu是否等于disk  "+cpu.equals(disk));
        System.out.println("cpu是否等于pc  "+cpu.equals(pc));
        System.out.println("disk是否等于pc  "+disk.equals(pc));
        String a=cpu.toString();
        System.out.println(a);
        String b=disk.toString();
        System.out.println(b);
        String c=cpu.toString();
        System.out.println(c);
    }
}
