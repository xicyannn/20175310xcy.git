/* @author xcy*/
/**生成setter、getter*/
public class Complex {

    double realPart;
    double imagePart;
    void setRealPart(double r){
        realPart =r;
    }
    void setImagePart(double i){
        imagePart =i;
    }
    double getRealPart(Complex a){
        return a.realPart;
    }
    double getImagePart(Complex a){
        return a.imagePart;
    }
    /**
    *定义构造函数
    *
    */
    public Complex(double r, double i){
        realPart = r;
        imagePart =i;
    }



    @Override
    public String toString(){
        String result = new String();
        if (imagePart >0) {
            result = realPart +"+"+ imagePart +"i";
        }
        if (imagePart ==0) {
            result = realPart +"";
        }
        if (imagePart <0) {
            result = realPart +"-"+ imagePart *(-1)+"i";
        }
        return result;
    }


    public Complex complexAdd(Complex a){
        return new Complex(realPart +a.realPart, imagePart +a.imagePart);
    }

    public Complex complexSub(Complex a){
        return new Complex(realPart -a.realPart, imagePart -a.imagePart);
    }

    public Complex complexMulti(Complex a){
        return new Complex(realPart *a.realPart - imagePart *a.imagePart, realPart *a.imagePart + imagePart *a.realPart);
    }

    public Complex complexDiv(Complex a){
        double d = Math.sqrt(a.realPart * a.realPart) + Math.sqrt(a.imagePart * a.imagePart);
        return new Complex((realPart*a.realPart+imagePart*a.imagePart)/d,(imagePart*a.realPart-realPart*a.imagePart)/d);
    }
}
