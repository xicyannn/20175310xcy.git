import java.math.BigDecimal;

public class Complex {
    double RealPart;
    double ImagePart;

    public Complex() {

    }

    public Complex(double R, double I) {
        RealPart = R;
        ImagePart = I;
    }

    public double getRealPart() {
        return RealPart;
    }

    public double getImagePart() {
        return ImagePart;
    }

    public void setImagePart(double i) {
        ImagePart = i;
    }

    public void setRealPart(double r) {
        RealPart = r;
    }

    public boolean equals(Object obj) {
        if (this.RealPart == ((Complex) obj).RealPart && this.ImagePart == ((Complex) obj).ImagePart) {
            return true;
        } else {
            return false;
        }
    }

    public String toString() {
        if (RealPart != 0.0) {
            if (ImagePart > 0.0) {
                return RealPart + "+" + ImagePart + "i";
            }
            if (ImagePart < 0.0) {
                return RealPart +""+ ImagePart + "i";
            } else {
                return String.valueOf(RealPart);
            }
        } else {
            return ImagePart + "i";
        }
    }

    Complex ComplexAdd(Complex a) {
        return new Complex(a.RealPart + this.RealPart, a.ImagePart + this.ImagePart);
    }

    Complex ComplexSub(Complex a) {
        return new Complex(RealPart - a.RealPart, ImagePart - a.ImagePart);
    }

    Complex ComplexMulti(Complex a) {
        return new Complex(this.RealPart * a.RealPart - this.ImagePart * a.ImagePart, this.RealPart * a.ImagePart + ImagePart * a.RealPart);
    }

    Complex ComplexDiv(Complex a) {
        BigDecimal bg = new BigDecimal((RealPart * a.ImagePart + ImagePart * a.RealPart) / (a.ImagePart * a.ImagePart + a.RealPart * a.RealPart));
        BigDecimal ag = new BigDecimal((ImagePart * a.ImagePart + RealPart * a.RealPart) / (a.ImagePart * a.ImagePart + a.RealPart * a.RealPart));
        return new Complex(ag.setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue(),bg.setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue());
    }
}
