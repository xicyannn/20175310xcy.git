import java.util.Scanner;
public class MyDCTest {

    public static void main(String[] args) {

        String expression, again;

        float result;

        try {
            Scanner in = new Scanner(System.in);

            do {
                MyDC evaluator = new MyDC();
                System.out.println("请输入后缀表达式: ");
                expression = in.nextLine();
                result = evaluator.calrp(expression);
                System.out.println("表达式的结果为：" + result);
                System.out.print("是否要计算下一个表达式 [Y/N]? ");
                again = in.nextLine();
                System.out.println();
            }
            while (again.equalsIgnoreCase("y"));
        } catch (Exception IOException) {
            System.out.println("输入格式错误！");
        }
    }
}

