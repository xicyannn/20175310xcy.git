import java.util.*;

public class MySort1 {
    public static void main(String [] args) {
        String [] toSort = {"aaa:10:1:1",
                            "ccc:30:3:4",
                            "bbb:50:4:5",
                            "ddd:20:5:3",
                            "eee:40:2:20"};

        System.out.println("Before sort:");
        for (String str: toSort) {
            System.out.println(str);
        }
        int [] a = new int[toSort.length];
        for (int i = 0; i < toSort.length; i++){
            String [] tmp = toSort[i].split(":");
            a[i] = Integer.parseInt(tmp[1]);
        }
        Arrays.sort(a);
        System.out.println("After sort:");
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < toSort.length; j++) {
                if (a[i] == Integer.parseInt((toSort[j].split(":"))[1])) {
                    System.out.println(toSort[j]);
                }
            }
        }
    }
}
