import java.util.*;
import junit.framework.TestCase;
import org.junit.Test;
public class array004Test extends TestCase {
    array004 x=new array004();
    int a[] = {4,32,45,64,65,85,2} ;
    int b[]={0};
    @Test
    public void testNormalsort(){     //sort测试正常情况
        Arrays.sort(a);
        assertEquals("2 4 32 45 64 65 85",x.paixu());
    }
    @Test
    public void testBoundarysort(){     //sort测试边界情况
        Arrays.sort(b);
        //assertEquals("0",x.paixu());
    }
    @Test
    public void testExceptionsort(){    //sort测试异常情况
        Arrays.sort(a);
        //assertEquals("4 2 32 45 64 65 85",x.paixu());
    }
    @Test
    public void testNormalbinarySearch(){    //binarySearch测试正常情况
        Arrays.sort(a);
        int index1 = Arrays.binarySearch(a,45);
        int index2 = Arrays.binarySearch(a,4);
        assertEquals("3",index1+"");
        assertEquals("1",index2+"");
    }
    @Test
    public void testBoundarybinarySearch(){     //binarySearch测试边界情况
        Arrays.sort(a);
        int index1 = Arrays.binarySearch(a,2);
        int index2 = Arrays.binarySearch(a,85);
        assertEquals("0",index1+"");
        assertEquals("6",index2+"");
    }
    @Test
    public void testExceptionbinarySearch(){     //binarySearch测试边界情况
        Arrays.sort(a);
        int index1 = Arrays.binarySearch(a,0);
        assertEquals("-1",index1+"");
    }
}
