public class ComplexDemo {
    public static void main(String[] a) {
    Complex b = new Complex(2, 6);
    Complex c = new Complex(1, -3);
    System.out.println("("+b + ")+(" + c + ")=" + b.ComplexAdd(c));
    System.out.println("("+b + ")-(" + c + ")=" + b.ComplexSub(c));
    System.out.println("("+b + ")*(" + c + ")=" + b.ComplexMulti(c));
    System.out.println("("+b + ")/(" + c + ")=" + b.ComplexDiv(c));
    }
}
