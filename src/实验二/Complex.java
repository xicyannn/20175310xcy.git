public class Complex {
    // 定义属性并生成getter,setter
    double RealPart;
    double ImagePart;
    void setRealPart(double r){
        RealPart=r;
    }
    void setImagePart(double i){
        ImagePart=i;
    }
    double getRealPart(Complex a){
        return a.RealPart;
    }
    double getImagePart(Complex a){
        return a.ImagePart;
    }
    // 定义构造函数
    public Complex(double r,double i){
        RealPart=r;
        ImagePart=i;
    }

    //Override Object

    public String toString(){
        String result = new String();
        if (ImagePart>0)
            result = RealPart+"+"+ImagePart+"i";
        if (ImagePart==0)
            result = RealPart+"";
        if (ImagePart<0)
            result = RealPart+"-"+ImagePart*(-1)+"i";
        return result;
    }


    public Complex ComplexAdd(Complex a){
        return new Complex(RealPart+a.RealPart,ImagePart+a.ImagePart);
    }

    public Complex ComplexSub(Complex a){
        return new Complex(RealPart-a.RealPart,ImagePart-a.ImagePart);
    }

    public Complex ComplexMulti(Complex a){
        return new Complex(RealPart*a.RealPart-ImagePart*a.ImagePart,RealPart*a.ImagePart+ImagePart*a.RealPart);
    }

    public Complex ComplexDiv(Complex a){
        double d = Math.sqrt(a.RealPart * a.RealPart) + Math.sqrt(a.ImagePart * a.ImagePart);
        return new Complex((RealPart*a.RealPart+ImagePart*a.ImagePart)/d,(ImagePart*a.RealPart-RealPart*a.ImagePart)/d);
    }
}
