import junit.framework.TestCase;
import org.junit.Test;
public class StringBufferDemoTest extends TestCase {
    StringBuffer a = new StringBuffer("hello");
    String testStr="h,e,l,l,o";
    @Test
    public void testNormalcharAt(){       //charAt正常情况
        assertEquals('e',a.charAt(1));
        assertEquals('l',a.charAt(2));
        assertEquals('l',a.charAt(3));
    }
    @Test
    public void testBoundarycharAt(){     //charAt边界情况
        assertEquals('h',a.charAt(0));
        assertEquals('o',a.charAt(4));
    }
    @Test
    public void testExceptionscharAt(){     //charAt异常情况
        //assertEquals('x',a.charAt(0));
        //assertEquals('y',a.charAt(4));
    }
    @Test
    public void testNormalsplit() {        //split正常情况
        String [] results =  testStr.split(",");
        assertEquals("e",results[1]);
        assertEquals("l",results[2]);
        assertEquals("o",results[4]);
    }
    @Test
    public void testBoundarysplit() {      //split边界情况
        String [] results =  testStr.split(",");
        assertEquals("h",results[0]);
        assertEquals("o",results[4]);
    }
    @Test
    public void testExceptionssplit() {        //split异常情况
        String [] results =  testStr.split(",");
        //assertEquals("A",results[0]);
        //assertEquals("B",results[1]);
        //assertEquals("C",results[2]);
    }
}
